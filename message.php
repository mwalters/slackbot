<?php
namespace Slack;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Message extends Command {

    protected function configure() {

        $this
            ->setName('message')
            ->setDescription('Sends messages')
            ->addArgument(
                'channel',
                InputArgument::REQUIRED,
                'Channel to send message to (with no `#`)'
            )
            ->addArgument(
                'message',
                InputArgument::REQUIRED,
                'Message to send to channel'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $channel = '#' . $input->getArgument('channel');
        $message = $input->getArgument('message');

        $this->sendMessage($channel, $message);

    }

    private function sendMessage($channel = '', $message = '') {
        global $slackApiToken,
               $slackBotName;

        $ch = curl_init("https://slack.com/api/channels.join");
        $data = http_build_query([
            "token" => $slackApiToken,
            "name" => $channel
        ]);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $ch = curl_init("https://slack.com/api/chat.postMessage");
        $data = http_build_query([
            "token" => $slackApiToken,
            "channel" => $channel,
            "text" => $message,
            "username" => $slackBotName,
            "icon_emoji" => ':alien:'
        ]);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
