Currently there is only one command implemented, the ability to send a message to a channel.

# Setup

Copy `config.php.sample` to `config.php` and edit it to enter your bots name and your slack API token.  The API token can be found here: [https://api.slack.com/web](https://api.slack.com/web), but you will need to log in first.

# Commands
`./bot message [channel] "[message]"`

Send [message] to [channel].  The channel name should **not** include the `#`.

# Example usage

`./bot message general "testing 1 2 3"`
